package vtc.project.instanthelpers;

import vtc.project.instanthelpers.EMF;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.api.server.spi.response.UnauthorizedException;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.users.User;
import com.google.appengine.datanucleus.query.JPACursorHelper;

import java.util.List;

import javax.annotation.Nullable;
import javax.inject.Named;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.persistence.EntityManager;
import javax.persistence.Query;

@Api(name = "ihmorderendpoint", version = "v1", scopes = { Ih_Constants.EMAIL_SCOPE }, clientIds = {
		Ih_Constants.WEB_CLIENT_ID, Ih_Constants.ANDROID_CLIENT_ID, Ih_Constants.IOS_CLIENT_ID,
		Ih_Constants.API_EXPLORER_CLIENT_ID }, audiences = {
				Ih_Constants.ANDROID_AUDIENCE }, namespace = @ApiNamespace(ownerDomain = "project.vtc", ownerName = "project.vtc", packagePath = "instanthelpers") )
public class IhMOrderEndpoint {

	/**
	 * This method lists all the entities inserted in datastore. It uses HTTP
	 * GET method and paging support.
	 *
	 * @return A CollectionResponse class containing the list of all entities
	 *         persisted and a cursor to the next page.
	 * @throws UnauthorizedException
	 */
	@SuppressWarnings({ "unchecked", "unused" })
	@ApiMethod(name = "listIhMOrder")
	public CollectionResponse<IhMOrder> listIhMOrder(@Nullable @Named("cursor") String cursorString,
			@Nullable @Named("limit") Integer limit, User user) throws UnauthorizedException {
		if (user == null)
			throw new UnauthorizedException("User is not valid");
		EntityManager mgr = null;
		Cursor cursor = null;
		List<IhMOrder> execute = null;

		try {
			mgr = getEntityManager();
			Query query = mgr.createQuery("select from IhMOrder as IhMOrder");
			if (cursorString != null && cursorString != "") {
				cursor = Cursor.fromWebSafeString(cursorString);
				query.setHint(JPACursorHelper.CURSOR_HINT, cursor);
			}

			if (limit != null) {
				query.setFirstResult(0);
				query.setMaxResults(limit);
			}

			execute = (List<IhMOrder>) query.getResultList();
			cursor = JPACursorHelper.getCursor(execute);
			if (cursor != null)
				cursorString = cursor.toWebSafeString();

			// Tight loop for fetching all entities from datastore and
			// accomodate
			// for lazy fetch.
			for (IhMOrder obj : execute)
				;
		} finally {
			mgr.close();
		}

		return CollectionResponse.<IhMOrder> builder().setItems(execute).setNextPageToken(cursorString).build();
	}

	@ApiMethod(name = "getIhMOrderByWorkerIdNew",path="byworkeridnew")
	public CollectionResponse<IhMOrder> getIhMOrderByWorkerId(@Nullable @Named("cursor") String cursorString,
			@Nullable @Named("limit") Integer limit, @Named("workerid") Long workerid, User user)
					throws UnauthorizedException {
		if (user == null)
			throw new UnauthorizedException("User is not valid");
		EntityManager mgr = null;
		Cursor cursor = null;
		List<IhMOrder> execute = null;

		try {
			mgr = getEntityManager();
			Query query = mgr
					.createQuery("select from IhMOrder as IhMOrder WHERE mWorkerId = :mWorkerId and mStatus = :mStatus order by timestamp desc");
			query.setParameter("mWorkerId", workerid);
			query.setParameter("mStatus", "Assigned");
			if (cursorString != null && cursorString != "") {
				cursor = Cursor.fromWebSafeString(cursorString);
				query.setHint(JPACursorHelper.CURSOR_HINT, cursor);
			}

			if (limit != null) {
				query.setFirstResult(0);
				query.setMaxResults(limit);
			}

			execute = (List<IhMOrder>) query.getResultList();
			cursor = JPACursorHelper.getCursor(execute);
			if (cursor != null)
				cursorString = cursor.toWebSafeString();

			// Tight loop for fetching all entities from datastore and
			// accomodate
			// for lazy fetch.
			for (IhMOrder obj : execute)
				;
		} finally {
			mgr.close();
		}
		return CollectionResponse.<IhMOrder> builder().setItems(execute).setNextPageToken(cursorString).build();
	}
	@ApiMethod(name = "getIhMOrderByWorkerIdCompleted",path="byworkeridcompleted")
	public CollectionResponse<IhMOrder> getIhMOrderByWorkerIdCompleted(@Nullable @Named("cursor") String cursorString,
			@Nullable @Named("limit") Integer limit, @Named("workerid") Long workerid, User user)
					throws UnauthorizedException {
		if (user == null)
			throw new UnauthorizedException("User is not valid");
		EntityManager mgr = null;
		Cursor cursor = null;
		List<IhMOrder> execute = null;

		try {
			mgr = getEntityManager();
			Query query = mgr
					.createQuery("select from IhMOrder as IhMOrder WHERE mWorkerId = :mWorkerId and mStatus = :mStatus order by timestamp desc");
			query.setParameter("mWorkerId", workerid);
			query.setParameter("mStatus", "Completed");
			if (cursorString != null && cursorString != "") {
				cursor = Cursor.fromWebSafeString(cursorString);
				query.setHint(JPACursorHelper.CURSOR_HINT, cursor);
			}

			if (limit != null) {
				query.setFirstResult(0);
				query.setMaxResults(limit);
			}

			execute = (List<IhMOrder>) query.getResultList();
			cursor = JPACursorHelper.getCursor(execute);
			if (cursor != null)
				cursorString = cursor.toWebSafeString();

			// Tight loop for fetching all entities from datastore and
			// accomodate
			// for lazy fetch.
			for (IhMOrder obj : execute)
				;
		} finally {
			mgr.close();
		}
		return CollectionResponse.<IhMOrder> builder().setItems(execute).setNextPageToken(cursorString).build();
	}

	@ApiMethod(name = "getIhMOrderByHelperIdNew",path="byhelperidnew")
	public CollectionResponse<IhMOrder> getIhMOrderByHelperId(@Nullable @Named("cursor") String cursorString,
			@Nullable @Named("limit") Integer limit, @Named("helperid") Long helperid, User user)
					throws UnauthorizedException {
		if (user == null)
			throw new UnauthorizedException("User is not valid");
		EntityManager mgr = null;
		Cursor cursor = null;
		List<IhMOrder> execute = null;

		try {
			mgr = getEntityManager();
			Query query = mgr
					.createQuery("select from IhMOrder as IhMOrder WHERE mHelperId = :mHelperId  and mStatus in (NULL, :mStatus)  order by timestamp desc");
			query.setParameter("mHelperId", helperid);
			query.setParameter("mStatus", "Assigned");
			if (cursorString != null && cursorString != "") {
				cursor = Cursor.fromWebSafeString(cursorString);
				query.setHint(JPACursorHelper.CURSOR_HINT, cursor);
			}

			if (limit != null) {
				query.setFirstResult(0);
				query.setMaxResults(limit);
			}

			execute = (List<IhMOrder>) query.getResultList();
			cursor = JPACursorHelper.getCursor(execute);
			if (cursor != null)
				cursorString = cursor.toWebSafeString();

			// Tight loop for fetching all entities from datastore and
			// accomodate
			// for lazy fetch.
			for (IhMOrder obj : execute)
				;
		} finally {
			mgr.close();
		}
		return CollectionResponse.<IhMOrder> builder().setItems(execute).setNextPageToken(cursorString).build();
	}
	@ApiMethod(name = "getIhMOrderByHelperIdCompleted",path="byhelperidcompleted")
	public CollectionResponse<IhMOrder> getIhMOrderByHelperIdCompleted(@Nullable @Named("cursor") String cursorString,
			@Nullable @Named("limit") Integer limit, @Named("helperid") Long helperid, User user)
					throws UnauthorizedException {
		if (user == null)
			throw new UnauthorizedException("User is not valid");
		EntityManager mgr = null;
		Cursor cursor = null;
		List<IhMOrder> execute = null;

		try {
			mgr = getEntityManager();
			Query query = mgr
					.createQuery("select from IhMOrder as IhMOrder WHERE mHelperId = :mHelperId  and mStatus = :mStatus order by timestamp desc");
			query.setParameter("mHelperId", helperid);
			query.setParameter("mStatus","Completed");
			if (cursorString != null && cursorString != "") {
				cursor = Cursor.fromWebSafeString(cursorString);
				query.setHint(JPACursorHelper.CURSOR_HINT, cursor);
			}

			if (limit != null) {
				query.setFirstResult(0);
				query.setMaxResults(limit);
			}

			execute = (List<IhMOrder>) query.getResultList();
			cursor = JPACursorHelper.getCursor(execute);
			if (cursor != null)
				cursorString = cursor.toWebSafeString();

			// Tight loop for fetching all entities from datastore and
			// accomodate
			// for lazy fetch.
			for (IhMOrder obj : execute)
				;
		} finally {
			mgr.close();
		}
		return CollectionResponse.<IhMOrder> builder().setItems(execute).setNextPageToken(cursorString).build();
	}

	/**
	 * This method gets the entity having primary key id. It uses HTTP GET
	 * method.
	 *
	 * @param id
	 *            the primary key of the java bean.
	 * @return The entity with primary key id.
	 * @throws UnauthorizedException
	 */
	@ApiMethod(name = "getIhMOrder")
	public IhMOrder getIhMOrder(@Named("id") Long id, User user) throws UnauthorizedException {
		if (user == null)
			throw new UnauthorizedException("User is not valid");
		EntityManager mgr = getEntityManager();
		IhMOrder ihmorder = null;
		try {
			ihmorder = mgr.find(IhMOrder.class, id);
		} finally {
			mgr.close();
		}
		return ihmorder;
	}

	/**
	 * This inserts a new entity into App Engine datastore. If the entity
	 * already exists in the datastore, an exception is thrown. It uses HTTP
	 * POST method.
	 *
	 * @param ihmorder
	 *            the entity to be inserted.
	 * @return The inserted entity.
	 * @throws UnauthorizedException
	 */
	@ApiMethod(name = "insertIhMOrder")
	public IhMOrder insertIhMOrder(IhMOrder ihmorder, User user) throws UnauthorizedException {
		if (user == null)
			throw new UnauthorizedException("User is not valid");
		EntityManager mgr = getEntityManager();
		try {
			if (ihmorder.get_id() != null) {
				if (containsIhMOrder(ihmorder)) {
					throw new EntityExistsException("Object already exists");
				}
			}
			mgr.persist(ihmorder);
		} finally {
			mgr.close();
		}
		return ihmorder;
	}

	/**
	 * This method is used for updating an existing entity. If the entity does
	 * not exist in the datastore, an exception is thrown. It uses HTTP PUT
	 * method.
	 *
	 * @param ihmorder
	 *            the entity to be updated.
	 * @return The updated entity.
	 * @throws UnauthorizedException
	 */
	@ApiMethod(name = "updateIhMOrder")
	public IhMOrder updateIhMOrder(IhMOrder ihmorder, User user) throws UnauthorizedException {
		if (user == null)
			throw new UnauthorizedException("User is not valid");
		EntityManager mgr = getEntityManager();
		try {
			if (!containsIhMOrder(ihmorder)) {
				throw new EntityNotFoundException("Object does not exist");
			}
			mgr.persist(ihmorder);
		} finally {
			mgr.close();
		}
		return ihmorder;
	}

	/**
	 * This method removes the entity with primary key id. It uses HTTP DELETE
	 * method.
	 *
	 * @param id
	 *            the primary key of the entity to be deleted.
	 * @throws UnauthorizedException
	 */
	@ApiMethod(name = "removeIhMOrder")
	public void removeIhMOrder(@Named("id") Long id, User user) throws UnauthorizedException {
		if (user == null)
			throw new UnauthorizedException("User is not valid");
		EntityManager mgr = getEntityManager();
		try {
			IhMOrder ihmorder = mgr.find(IhMOrder.class, id);
			mgr.remove(ihmorder);
		} finally {
			mgr.close();
		}
	}

	private boolean containsIhMOrder(IhMOrder ihmorder) {
		EntityManager mgr = getEntityManager();
		boolean contains = true;
		try {
			IhMOrder item = mgr.find(IhMOrder.class, ihmorder.get_id());
			if (item == null) {
				contains = false;
			}
		} finally {
			mgr.close();
		}
		return contains;
	}

	private static EntityManager getEntityManager() {
		return EMF.get().createEntityManager();
	}

}
