package vtc.project.instanthelpers;

import vtc.project.instanthelpers.EMF;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.api.server.spi.response.UnauthorizedException;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.users.User;
import com.google.appengine.datanucleus.query.JPACursorHelper;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.annotation.Nullable;
import javax.inject.Named;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.persistence.EntityManager;
import javax.persistence.Query;

@Api(name = "ihmrequestendpoint", version = "v1", scopes = { Ih_Constants.EMAIL_SCOPE }, clientIds = {
		Ih_Constants.WEB_CLIENT_ID, Ih_Constants.ANDROID_CLIENT_ID,
		Ih_Constants.IOS_CLIENT_ID, Ih_Constants.API_EXPLORER_CLIENT_ID }, audiences = { Ih_Constants.ANDROID_AUDIENCE }, namespace = @ApiNamespace(ownerDomain = "project.vtc", ownerName = "project.vtc", packagePath = "instanthelpers"))
public class IhMRequestEndpoint {

	/**
	 * This method lists all the entities inserted in datastore. It uses HTTP
	 * GET method and paging support.
	 *
	 * @return A CollectionResponse class containing the list of all entities
	 *         persisted and a cursor to the next page.
	 * @throws UnauthorizedException 
	 */
	@SuppressWarnings({ "unchecked", "unused" })
	@ApiMethod(name = "listIhMRequest")
	public CollectionResponse<IhMRequest> listIhMRequest(
			@Nullable @Named("cursor") String cursorString,
			@Nullable @Named("limit") Integer limit, User user) throws UnauthorizedException {
		if(user == null) throw new UnauthorizedException("User is not valid");
		EntityManager mgr = null;
		Cursor cursor = null;
		List<IhMRequest> execute = null;

		try {
			mgr = getEntityManager();
			Query query = mgr
					.createQuery("select from IhMRequest as IhMRequest");
			if (cursorString != null && cursorString != "") {
				cursor = Cursor.fromWebSafeString(cursorString);
				query.setHint(JPACursorHelper.CURSOR_HINT, cursor);
			}

			if (limit != null) {
				query.setFirstResult(0);
				query.setMaxResults(limit);
			}

			execute = (List<IhMRequest>) query.getResultList();
			cursor = JPACursorHelper.getCursor(execute);
			if (cursor != null)
				cursorString = cursor.toWebSafeString();

			// Tight loop for fetching all entities from datastore and
			// accomodate
			// for lazy fetch.
			for (IhMRequest obj : execute)
				;
		} finally {
			mgr.close();
		}

		return CollectionResponse.<IhMRequest> builder().setItems(execute)
				.setNextPageToken(cursorString).build();
	}

	@ApiMethod(name = "getIhMRequestByCustomerId",path="bycustomerid")
	public CollectionResponse<IhMRequest> getIhMRequestByCustomer(
			@Nullable @Named("cursor") String cursorString,
			@Nullable @Named("limit") Integer limit, 
			@Named("customerid") Long customerid, User user) throws UnauthorizedException {
		if(user == null) throw new UnauthorizedException("User is not valid");
		EntityManager mgr = null;
		Cursor cursor = null;
		List<IhMRequest> execute = null;

		try {
			mgr = getEntityManager();
			Query query = mgr
					.createQuery("select from IhMRequest as IhMRequest WHERE mCustomerId = :mCustomerId order by timestamp desc");
			query.setParameter("mCustomerId", customerid);
			if (cursorString != null && cursorString != "") {
				cursor = Cursor.fromWebSafeString(cursorString);
				query.setHint(JPACursorHelper.CURSOR_HINT, cursor);
			}

			if (limit != null) {
				query.setFirstResult(0);
				query.setMaxResults(limit);
			}

			execute = (List<IhMRequest>) query.getResultList();
			cursor = JPACursorHelper.getCursor(execute);
			if (cursor != null)
				cursorString = cursor.toWebSafeString();

			// Tight loop for fetching all entities from datastore and
			// accomodate
			// for lazy fetch.
			for (IhMRequest obj : execute)
				;
		} finally {
			mgr.close();
		}

		return CollectionResponse.<IhMRequest> builder().setItems(execute)
				.setNextPageToken(cursorString).build();
	}

	@ApiMethod(name = "getIhMRequestNotConfirmed",path="notconfirmed")
	public CollectionResponse<IhMRequest> getIhMRequestNotConfirmed(
			@Nullable @Named("cursor") String cursorString,
			@Nullable @Named("limit") Integer limit, User user) throws UnauthorizedException {
		if(user == null) throw new UnauthorizedException("User is not valid");
		EntityManager mgr = null;
		Cursor cursor = null;
		List<IhMRequest> execute = null;

		try {
			mgr = getEntityManager();
			Query query = mgr
					.createQuery("select from IhMRequest as IhMRequest WHERE mQuotationId = NULL");

			if (cursorString != null && cursorString != "") {
				cursor = Cursor.fromWebSafeString(cursorString);
				query.setHint(JPACursorHelper.CURSOR_HINT, cursor);
			}

			if (limit != null) {
				query.setFirstResult(0);
				query.setMaxResults(limit);
			}

			execute = (List<IhMRequest>) query.getResultList();
			cursor = JPACursorHelper.getCursor(execute);
			if (cursor != null)
				cursorString = cursor.toWebSafeString();
			
		    if (execute.size() > 0) {
		        Collections.sort(execute, new Comparator<IhMRequest>() {
		            @Override
		            public int compare(final IhMRequest object1, final IhMRequest object2) {
		                return Long.compare(object1.getTimestamp(),object2.getTimestamp());
		            }
		           } );
		       }

			// Tight loop for fetching all entities from datastore and
			// accomodate
			// for lazy fetch.
			for (IhMRequest obj : execute)
				;
				
		} finally {
			mgr.close();
		}

		return CollectionResponse.<IhMRequest> builder().setItems(execute)
				.setNextPageToken(cursorString).build();
	}

	/**
	 * This method gets the entity having primary key id. It uses HTTP GET
	 * method.
	 *
	 * @param id
	 *            the primary key of the java bean.
	 * @return The entity with primary key id.
	 * @throws UnauthorizedException 
	 */
	@ApiMethod(name = "getIhMRequest")
	public IhMRequest getIhMRequest(@Named("id") Long id, User user) throws UnauthorizedException {
		if(user == null) throw new UnauthorizedException("User is not valid");
		EntityManager mgr = getEntityManager();
		IhMRequest ihmrequest = null;
		try {
			ihmrequest = mgr.find(IhMRequest.class, id);
		} finally {
			mgr.close();
		}
		return ihmrequest;
	}

	/**
	 * This inserts a new entity into App Engine datastore. If the entity
	 * already exists in the datastore, an exception is thrown. It uses HTTP
	 * POST method.
	 *
	 * @param ihmrequest
	 *            the entity to be inserted.
	 * @return The inserted entity.
	 * @throws UnauthorizedException 
	 */
	@ApiMethod(name = "insertIhMRequest")
	public IhMRequest insertIhMRequest(IhMRequest ihmrequest, User user) throws UnauthorizedException {
		if(user == null) throw new UnauthorizedException("User is not valid");
		EntityManager mgr = getEntityManager();
		try {
			if (ihmrequest.get_id() != null) {
				if (containsIhMRequest(ihmrequest)) {
					throw new EntityExistsException("Object already exists");
				}
			}
			mgr.persist(ihmrequest);
		} finally {
			mgr.close();
		}
		return ihmrequest;
	}

	/**
	 * This method is used for updating an existing entity. If the entity does
	 * not exist in the datastore, an exception is thrown. It uses HTTP PUT
	 * method.
	 *
	 * @param ihmrequest
	 *            the entity to be updated.
	 * @return The updated entity.
	 * @throws UnauthorizedException 
	 */
	@ApiMethod(name = "updateIhMRequest")
	public IhMRequest updateIhMRequest(IhMRequest ihmrequest, User user) throws UnauthorizedException {
		if(user == null) throw new UnauthorizedException("User is not valid");
		EntityManager mgr = getEntityManager();
		try {
			if (!containsIhMRequest(ihmrequest)) {
				throw new EntityNotFoundException("Object does not exist");
			}
			mgr.persist(ihmrequest);
		} finally {
			mgr.close();
		}
		return ihmrequest;
	}

	/**
	 * This method removes the entity with primary key id. It uses HTTP DELETE
	 * method.
	 *
	 * @param id
	 *            the primary key of the entity to be deleted.
	 * @throws UnauthorizedException 
	 */
	@ApiMethod(name = "removeIhMRequest")
	public void removeIhMRequest(@Named("id") Long id, User user) throws UnauthorizedException {
		if(user == null) throw new UnauthorizedException("User is not valid");
		EntityManager mgr = getEntityManager();
		try {
			IhMRequest ihmrequest = mgr.find(IhMRequest.class, id);
			mgr.remove(ihmrequest);
		} finally {
			mgr.close();
		}
	}

	private boolean containsIhMRequest(IhMRequest ihmrequest) {
		EntityManager mgr = getEntityManager();
		boolean contains = true;
		try {
			IhMRequest item = mgr.find(IhMRequest.class, ihmrequest.get_id());
			if (item == null) {
				contains = false;
			}
		} finally {
			mgr.close();
		}
		return contains;
	}

	private static EntityManager getEntityManager() {
		return EMF.get().createEntityManager();
	}

}
