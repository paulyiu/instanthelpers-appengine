package vtc.project.instanthelpers;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Nullable;
import javax.inject.Named;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.api.server.spi.response.UnauthorizedException;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.users.User;
import com.google.appengine.datanucleus.query.JPACursorHelper;

@Api(name = "ihmworkerendpoint", version = "v1", scopes = { Ih_Constants.EMAIL_SCOPE }, clientIds = {
		Ih_Constants.WEB_CLIENT_ID, Ih_Constants.ANDROID_CLIENT_ID,
		Ih_Constants.IOS_CLIENT_ID, Ih_Constants.API_EXPLORER_CLIENT_ID }, audiences = { Ih_Constants.ANDROID_AUDIENCE }, namespace = @ApiNamespace(ownerDomain = "project.vtc", ownerName = "project.vtc", packagePath = "instanthelpers"))
public class IhMWorkerEndpoint {

	/**
	 * This method lists all the entities inserted in datastore. It uses HTTP
	 * GET method and paging support.
	 *
	 * @return A CollectionResponse class containing the list of all entities
	 *         persisted and a cursor to the next page.
	 * @throws UnauthorizedException 
	 */
	@SuppressWarnings({ "unchecked", "unused" })
	@ApiMethod(name = "listIhMWorker")
	public CollectionResponse<IhMWorker> listIhMWorker(
			@Nullable @Named("cursor") String cursorString,
			@Nullable @Named("limit") Integer limit, User user) throws UnauthorizedException {
		if(user == null) throw new UnauthorizedException("User is not valid");
		EntityManager mgr = null;
		Cursor cursor = null;
		List<IhMWorker> execute = null;

		try {
			mgr = getEntityManager();
			Query query = mgr.createQuery("select from IhMWorker as IhMWorker");
			if (cursorString != null && cursorString != "") {
				cursor = Cursor.fromWebSafeString(cursorString);
				query.setHint(JPACursorHelper.CURSOR_HINT, cursor);
			}

			if (limit != null) {
				query.setFirstResult(0);
				query.setMaxResults(limit);
			}

			execute = (List<IhMWorker>) query.getResultList();
			cursor = JPACursorHelper.getCursor(execute);
			if (cursor != null)
				cursorString = cursor.toWebSafeString();

			// Tight loop for fetching all entities from datastore and
			// accomodate
			// for lazy fetch.
			for (IhMWorker obj : execute)
				;
		} finally {
			mgr.close();
		}

		return CollectionResponse.<IhMWorker> builder().setItems(execute)
				.setNextPageToken(cursorString).build();
	}

	/**
	 * This method gets the entity having primary key id. It uses HTTP GET
	 * method.
	 *
	 * @param id
	 *            the primary key of the java bean.
	 * @return The entity with primary key id.
	 * @throws UnauthorizedException 
	 */
	@ApiMethod(name = "getIhMWorker")
	public IhMWorker getIhMWorker(@Named("id") Long id, User user) throws UnauthorizedException {
		if(user == null) throw new UnauthorizedException("User is not valid");
		EntityManager mgr = getEntityManager();
		IhMWorker ihmworker = null;
		try {
			ihmworker = mgr.find(IhMWorker.class, id);
		} finally {
			mgr.close();
		}
		return ihmworker;
	}

	/**
	 * This inserts a new entity into App Engine datastore. If the entity
	 * already exists in the datastore, an exception is thrown. It uses HTTP
	 * POST method.
	 *
	 * @param ihmworker
	 *            the entity to be inserted.
	 * @return The inserted entity.
	 * @throws UnauthorizedException 
	 */
	@ApiMethod(name = "insertIhMWorker")
	public IhMWorker insertIhMWorker(IhMWorker ihmworker,User user) throws UnauthorizedException {
		if(user == null) throw new UnauthorizedException("User is not valid");
		EntityManager mgr = getEntityManager();
		try {
			if (ihmworker.get_id() != null) {
				if (containsIhMWorker(ihmworker)) {
					throw new EntityExistsException("Object already exists");
				}
			}
			mgr.persist(ihmworker);
		} finally {
			mgr.close();
		}
		return ihmworker;
	}
	@ApiMethod(name = "getIhMWorkerByHelperId",path="getIhMWorkerByHelperId")
	public CollectionResponse<IhMWorker> getIhMWorkerByHelperId(
			@Nullable @Named("cursor") String cursorString,
			@Nullable @Named("limit") Integer limit,@Named("helperid") Long helperid, User user) throws UnauthorizedException {
		if(user == null) throw new UnauthorizedException("User is not valid");
		EntityManager mgr = null;
		Cursor cursor = null;
		List<IhMWorker> execute = null;

		try {
			mgr = getEntityManager();
			Query query = mgr.createQuery("select from IhMWorker as IhMWorker WHERE mHelperId = :mHelperId");
			query.setParameter("mHelperId", helperid);
			if (cursorString != null && cursorString != "") {
				cursor = Cursor.fromWebSafeString(cursorString);
				query.setHint(JPACursorHelper.CURSOR_HINT, cursor);
			}

			if (limit != null) {
				query.setFirstResult(0);
				query.setMaxResults(limit);
			}

			execute = (List<IhMWorker>) query.getResultList();
			cursor = JPACursorHelper.getCursor(execute);
			if (cursor != null)
				cursorString = cursor.toWebSafeString();

			// Tight loop for fetching all entities from datastore and
			// accomodate
			// for lazy fetch.
			for (IhMWorker obj : execute)
				;
		} finally {
			mgr.close();
		}

		return CollectionResponse.<IhMWorker> builder().setItems(execute)
				.setNextPageToken(cursorString).build();
	}

	/**
	 * This method is used for updating an existing entity. If the entity does
	 * not exist in the datastore, an exception is thrown. It uses HTTP PUT
	 * method.
	 *
	 * @param ihmworker
	 *            the entity to be updated.
	 * @return The updated entity.
	 * @throws UnauthorizedException 
	 */
	@ApiMethod(name = "updateIhMWorker")
	public IhMWorker updateIhMWorker(IhMWorker ihmworker, User user) throws UnauthorizedException {
		if(user == null) throw new UnauthorizedException("User is not valid");
		EntityManager mgr = getEntityManager();
		try {
			if (!containsIhMWorker(ihmworker)) {
				throw new EntityNotFoundException("Object does not exist");
			}
			mgr.persist(ihmworker);
		} finally {
			mgr.close();
		}
		return ihmworker;
	}

	/**
	 * This method removes the entity with primary key id. It uses HTTP DELETE
	 * method.
	 *
	 * @param id
	 *            the primary key of the entity to be deleted.
	 * @throws UnauthorizedException 
	 */
	@ApiMethod(name = "removeIhMWorker")
	public void removeIhMWorker(@Named("id") Long id, User user) throws UnauthorizedException {
		if(user == null) throw new UnauthorizedException("User is not valid");
		EntityManager mgr = getEntityManager();
		try {
			IhMWorker ihmworker = mgr.find(IhMWorker.class, id);
			mgr.remove(ihmworker);
		} finally {
			mgr.close();
		}
	}

	private boolean containsIhMWorker(IhMWorker ihmworker) {
		EntityManager mgr = getEntityManager();
		boolean contains = true;
		try {
			IhMWorker item = mgr.find(IhMWorker.class, ihmworker.get_id());
			if (item == null) {
				contains = false;
			}
		} finally {
			mgr.close();
		}
		return contains;
	}

	private static EntityManager getEntityManager() {
		return EMF.get().createEntityManager();
	}

}
