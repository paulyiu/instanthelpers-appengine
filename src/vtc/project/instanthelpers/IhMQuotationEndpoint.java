package vtc.project.instanthelpers;

import vtc.project.instanthelpers.EMF;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.api.server.spi.response.UnauthorizedException;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.users.User;
import com.google.appengine.datanucleus.query.JPACursorHelper;

import java.util.List;

import javax.annotation.Nullable;
import javax.inject.Named;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.persistence.EntityManager;
import javax.persistence.Query;

@Api(name = "ihmquotationendpoint", version = "v1", scopes = { Ih_Constants.EMAIL_SCOPE }, clientIds = {
		Ih_Constants.WEB_CLIENT_ID, Ih_Constants.ANDROID_CLIENT_ID,
		Ih_Constants.IOS_CLIENT_ID, Ih_Constants.API_EXPLORER_CLIENT_ID }, audiences = { Ih_Constants.ANDROID_AUDIENCE }, namespace = @ApiNamespace(ownerDomain = "project.vtc", ownerName = "project.vtc", packagePath = "instanthelpers"))
public class IhMQuotationEndpoint {

	/**
	 * This method lists all the entities inserted in datastore. It uses HTTP
	 * GET method and paging support.
	 *
	 * @return A CollectionResponse class containing the list of all entities
	 *         persisted and a cursor to the next page.
	 * @throws UnauthorizedException 
	 */
	@SuppressWarnings({ "unchecked", "unused" })
	@ApiMethod(name = "listIhMQuotation")
	public CollectionResponse<IhMQuotation> listIhMQuotation(
			@Nullable @Named("cursor") String cursorString,
			@Nullable @Named("limit") Integer limit, User user) throws UnauthorizedException {
		if(user == null) throw new UnauthorizedException("User is not valid");
		EntityManager mgr = null;
		Cursor cursor = null;
		List<IhMQuotation> execute = null;

		try {
			mgr = getEntityManager();
			Query query = mgr
					.createQuery("select from IhMQuotation as IhMQuotation");
			if (cursorString != null && cursorString != "") {
				cursor = Cursor.fromWebSafeString(cursorString);
				query.setHint(JPACursorHelper.CURSOR_HINT, cursor);
			}

			if (limit != null) {
				query.setFirstResult(0);
				query.setMaxResults(limit);
			}

			execute = (List<IhMQuotation>) query.getResultList();
			cursor = JPACursorHelper.getCursor(execute);
			if (cursor != null)
				cursorString = cursor.toWebSafeString();

			// Tight loop for fetching all entities from datastore and
			// accomodate
			// for lazy fetch.
			for (IhMQuotation obj : execute)
				;
		} finally {
			mgr.close();
		}

		return CollectionResponse.<IhMQuotation> builder().setItems(execute)
				.setNextPageToken(cursorString).build();
	}
	@ApiMethod(name = "getIhMQuotationByRequestId",path="requestid")
	public CollectionResponse<IhMQuotation> getIhMQuotationByRequestId(
			@Nullable @Named("cursor") String cursorString,
			@Nullable @Named("limit") Integer limit,@Named("requestid") Long requestid, User user) throws UnauthorizedException {
		if(user == null) throw new UnauthorizedException("User is not valid");
		EntityManager mgr = null;
		Cursor cursor = null;
		List<IhMQuotation> execute = null;

		try {
			mgr = getEntityManager();
			Query query = mgr
					.createQuery("select from IhMQuotation as IhMQuotation WHERE mRequestId = :mRequestId order by timestamp");
			query.setParameter("mRequestId", requestid);
			if (cursorString != null && cursorString != "") {
				cursor = Cursor.fromWebSafeString(cursorString);
				query.setHint(JPACursorHelper.CURSOR_HINT, cursor);
			}

			if (limit != null) {
				query.setFirstResult(0);
				query.setMaxResults(limit);
			}

			execute = (List<IhMQuotation>) query.getResultList();
			cursor = JPACursorHelper.getCursor(execute);
			if (cursor != null)
				cursorString = cursor.toWebSafeString();

			// Tight loop for fetching all entities from datastore and
			// accomodate
			// for lazy fetch.
			for (IhMQuotation obj : execute)
				;
		} finally {
			mgr.close();
		}

		return CollectionResponse.<IhMQuotation> builder().setItems(execute)
				.setNextPageToken(cursorString).build();
	}

	@ApiMethod(name = "getIhMQuotationByHelperId",path="byhelperid")
	public CollectionResponse<IhMQuotation> getIhMQuotationByHelperId(
			@Nullable @Named("cursor") String cursorString,
			@Nullable @Named("limit") Integer limit,@Named("helperid") Long helperid, User user) throws UnauthorizedException {
		if(user == null) throw new UnauthorizedException("User is not valid");
		EntityManager mgr = null;
		Cursor cursor = null;
		List<IhMQuotation> execute = null;

		try {
			mgr = getEntityManager();
			Query query = mgr
					.createQuery("select from IhMQuotation as IhMQuotation WHERE _id = :mHelperId order by timestamp");
			query.setParameter("mHelperId", helperid);
			if (cursorString != null && cursorString != "") {
				cursor = Cursor.fromWebSafeString(cursorString);
				query.setHint(JPACursorHelper.CURSOR_HINT, cursor);
			}

			if (limit != null) {
				query.setFirstResult(0);
				query.setMaxResults(limit);
			}

			execute = (List<IhMQuotation>) query.getResultList();
			cursor = JPACursorHelper.getCursor(execute);
			if (cursor != null)
				cursorString = cursor.toWebSafeString();

			// Tight loop for fetching all entities from datastore and
			// accomodate
			// for lazy fetch.
			for (IhMQuotation obj : execute)
				;
		} finally {
			mgr.close();
		}

		return CollectionResponse.<IhMQuotation> builder().setItems(execute)
				.setNextPageToken(cursorString).build();
	}
	@ApiMethod(name = "getIhMQuotationByRequestAndHelperId",path="requestandhelperid")
	public CollectionResponse<IhMQuotation> getIhMQuotationByRequestAndHelperId(
			@Nullable @Named("cursor") String cursorString,
			@Nullable @Named("limit") Integer limit,@Named("requestid") Long requestid, @Named("helperid") Long helperid,User user) throws UnauthorizedException {
		if(user == null) throw new UnauthorizedException("User is not valid");
		EntityManager mgr = null;
		Cursor cursor = null;
		List<IhMQuotation> execute = null;

		try {
			mgr = getEntityManager();
			Query query = mgr
					.createQuery("select from IhMQuotation as IhMQuotation WHERE mRequestId = :mRequestId and mHelperId = :mHelperId order by timestamp");
			query.setParameter("mRequestId", requestid);
			query.setParameter("mHelperId",helperid);
			if (cursorString != null && cursorString != "") {
				cursor = Cursor.fromWebSafeString(cursorString);
				query.setHint(JPACursorHelper.CURSOR_HINT, cursor);
			}

			if (limit != null) {
				query.setFirstResult(0);
				query.setMaxResults(limit);
			}

			execute = (List<IhMQuotation>) query.getResultList();
			cursor = JPACursorHelper.getCursor(execute);
			if (cursor != null)
				cursorString = cursor.toWebSafeString();

			// Tight loop for fetching all entities from datastore and
			// accomodate
			// for lazy fetch.
			for (IhMQuotation obj : execute)
				;
		} finally {
			mgr.close();
		}

		return CollectionResponse.<IhMQuotation> builder().setItems(execute)
				.setNextPageToken(cursorString).build();
	}

	/**
	 * This method gets the entity having primary key id. It uses HTTP GET
	 * method.
	 *
	 * @param id
	 *            the primary key of the java bean.
	 * @return The entity with primary key id.
	 * @throws UnauthorizedException 
	 */
	@ApiMethod(name = "getIhMQuotation")
	public IhMQuotation getIhMQuotation(@Named("id") Long id, User user) throws UnauthorizedException {
		if(user == null) throw new UnauthorizedException("User is not valid");
		EntityManager mgr = getEntityManager();
		IhMQuotation ihmquotation = null;
		try {
			ihmquotation = mgr.find(IhMQuotation.class, id);
		} finally {
			mgr.close();
		}
		return ihmquotation;
	}

	/**
	 * This inserts a new entity into App Engine datastore. If the entity
	 * already exists in the datastore, an exception is thrown. It uses HTTP
	 * POST method.
	 *
	 * @param ihmquotation
	 *            the entity to be inserted.
	 * @return The inserted entity.
	 * @throws UnauthorizedException 
	 */
	@ApiMethod(name = "insertIhMQuotation")
	public IhMQuotation insertIhMQuotation(IhMQuotation ihmquotation, User user) throws UnauthorizedException {
		if(user == null) throw new UnauthorizedException("User is not valid");
		EntityManager mgr = getEntityManager();
		try {
			if (ihmquotation.get_id() != null) {
				if (containsIhMQuotation(ihmquotation)) {
					throw new EntityExistsException("Object already exists");
				}
			}
			mgr.persist(ihmquotation);
		} finally {
			mgr.close();
		}
		return ihmquotation;
	}

	/**
	 * This method is used for updating an existing entity. If the entity does
	 * not exist in the datastore, an exception is thrown. It uses HTTP PUT
	 * method.
	 *
	 * @param ihmquotation
	 *            the entity to be updated.
	 * @return The updated entity.
	 * @throws UnauthorizedException 
	 */
	@ApiMethod(name = "updateIhMQuotation")
	public IhMQuotation updateIhMQuotation(IhMQuotation ihmquotation, User user) throws UnauthorizedException {
		if(user == null) throw new UnauthorizedException("User is not valid");
		EntityManager mgr = getEntityManager();
		try {
			if (!containsIhMQuotation(ihmquotation)) {
				throw new EntityNotFoundException("Object does not exist");
			}
			mgr.persist(ihmquotation);
		} finally {
			mgr.close();
		}
		return ihmquotation;
	}

	/**
	 * This method removes the entity with primary key id. It uses HTTP DELETE
	 * method.
	 *
	 * @param id
	 *            the primary key of the entity to be deleted.
	 * @throws UnauthorizedException 
	 */
	@ApiMethod(name = "removeIhMQuotation")
	public void removeIhMQuotation(@Named("id") Long id, User user) throws UnauthorizedException {
		if(user == null) throw new UnauthorizedException("User is not valid");
		EntityManager mgr = getEntityManager();
		try {
			IhMQuotation ihmquotation = mgr.find(IhMQuotation.class, id);
			mgr.remove(ihmquotation);
		} finally {
			mgr.close();
		}
	}

	private boolean containsIhMQuotation(IhMQuotation ihmquotation) {
		EntityManager mgr = getEntityManager();
		boolean contains = true;
		try {
			IhMQuotation item = mgr.find(IhMQuotation.class,
					ihmquotation.get_id());
			if (item == null) {
				contains = false;
			}
		} finally {
			mgr.close();
		}
		return contains;
	}

	private static EntityManager getEntityManager() {
		return EMF.get().createEntityManager();
	}

}
