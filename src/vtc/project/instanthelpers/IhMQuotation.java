package vtc.project.instanthelpers;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class IhMQuotation {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long _id;
	private Long mRequestId;
	private Long mHelperId;
	private int mPrice;
	private String mRemark;
	private long timestamp = System.currentTimeMillis();

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public IhMQuotation() {
		super();
	}

	public Long get_id() {
		return _id;
	}

	public Long getmRequestId() {
		return mRequestId;
	}
	public void setmRequestId(Long mRequestId) {
		this.mRequestId = mRequestId;
	}
	public Long getmHelperId() {
		return mHelperId;
	}
	public void setmHelperId(Long mHelperId) {
		this.mHelperId = mHelperId;
	}
	public int getmPrice() {
		return mPrice;
	}
	public void setmPrice(int mPrice) {
		this.mPrice = mPrice;
	}
	public String getmRemark() {
		return mRemark;
	}
	public void setmRemark(String mRemark) {
		this.mRemark = mRemark;
	}
	
}
