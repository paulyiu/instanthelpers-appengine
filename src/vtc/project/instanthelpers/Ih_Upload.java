package vtc.project.instanthelpers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;

public class Ih_Upload extends HttpServlet {

	private final static Logger _logger = Logger.getLogger(Ih_Upload.class
			.getName());
	private BlobstoreService blobstoreService = BlobstoreServiceFactory
			.getBlobstoreService();

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		Map<String, BlobKey> blobs = blobstoreService.getUploadedBlobs(req);
		BlobKey blobKey = blobs.get("blobImg");
		PrintWriter out = res.getWriter();
		if(blobKey == null){
			//res.sendRedirect("/");
			out.println("error: null blob key");
		}else{
			String strStatus = "";
			
				String url = "/serve?blob-key="+blobKey.getKeyString();
				System.out.println(url);
			//res.sendRedirect(url);
				out.println(blobKey.getKeyString());
		}
	}
}
