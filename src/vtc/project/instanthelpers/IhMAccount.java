package vtc.project.instanthelpers;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class IhMAccount {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long _id;
	private String mEmail;
	private String mNickName;
	private String mRegisteredDevice;
	private String mAccountType;
	private long timestamp = System.currentTimeMillis();

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public IhMAccount() {
		super();
	}

	public String getmAccountType() {
		return mAccountType;
	}

	public void setmAccountType(String mAccountType) {
		this.mAccountType = mAccountType;
	}

	public String getmRegisteredDevice() {
		return mRegisteredDevice;
	}

	public void setmRegisteredDevice(String mRegisteredDevice) {
		this.mRegisteredDevice = mRegisteredDevice;
	}

	public String getmEmail() {
		return mEmail;
	}

	public void setmEmail(String mEmail) {
		this.mEmail = mEmail;
	}

	public String getmNickName() {
		return mNickName;
	}

	public void setmNickName(String mNickName) {
		this.mNickName = mNickName;
	}

	public Long get_id() {
		return _id;
	}
	
}
