package vtc.project.instanthelpers;

import java.util.ArrayList;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class IhMRequest {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long _id;
	private Long mCustomerId;
	private Long mOrderId;
	private Long mQuotationId;
	private int mOrderType;
	private String mContractNumber;
	private String mRegion;
	private String mAddress;
	private String mComment;
	private long timestamp = System.currentTimeMillis();

	public Long getmQuotationId() {
		return mQuotationId;
	}

	public void setmQuotationId(Long mQuotationId) {
		this.mQuotationId = mQuotationId;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	private ArrayList<String> mPhoto;

	public IhMRequest() {
		super();
	}

	public Long get_id() {
		return _id;
	}

	public Long getmCustomerId() {
		return mCustomerId;
	}

	public void setmCustomerId(Long mCustomerId) {
		this.mCustomerId = mCustomerId;
	}

	public Long getmOrderId() {
		return mOrderId;
	}

	public void setmOrderId(Long mOrderId) {
		this.mOrderId = mOrderId;
	}

	public int getmOrderType() {
		return mOrderType;
	}

	public void setmOrderType(int mOrderType) {
		this.mOrderType = mOrderType;
	}

	public String getmContractNumber() {
		return mContractNumber;
	}

	public void setmContractNumber(String mContractNumber) {
		this.mContractNumber = mContractNumber;
	}

	public String getmRegion() {
		return mRegion;
	}

	public void setmRegion(String mRegion) {
		this.mRegion = mRegion;
	}

	public String getmAddress() {
		return mAddress;
	}

	public void setmAddress(String mAddress) {
		this.mAddress = mAddress;
	}

	public String getmComment() {
		return mComment;
	}

	public void setmComment(String mComment) {
		this.mComment = mComment;
	}

	public ArrayList<String> getmPhoto() {
		return mPhoto;
	}

	public void setmPhoto(ArrayList<String> mPhoto) {
		this.mPhoto = mPhoto;
	}

}
