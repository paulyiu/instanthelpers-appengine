package vtc.project.instanthelpers;

import vtc.project.instanthelpers.EMF;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.api.server.spi.response.UnauthorizedException;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.users.User;
import com.google.appengine.datanucleus.query.JPACursorHelper;

import java.util.List;

import javax.annotation.Nullable;
import javax.inject.Named;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.persistence.EntityManager;
import javax.persistence.Query;

@Api(name = "ihmcustomerendpoint", version = "v1", scopes = { Ih_Constants.EMAIL_SCOPE }, clientIds = {
		Ih_Constants.WEB_CLIENT_ID, Ih_Constants.ANDROID_CLIENT_ID,
		Ih_Constants.IOS_CLIENT_ID, Ih_Constants.API_EXPLORER_CLIENT_ID }, audiences = { Ih_Constants.ANDROID_AUDIENCE }, namespace = @ApiNamespace(ownerDomain = "project.vtc", ownerName = "project.vtc", packagePath = "instanthelpers"))
public class IhMCustomerEndpoint {

	/**
	 * This method lists all the entities inserted in datastore. It uses HTTP
	 * GET method and paging support.
	 *
	 * @return A CollectionResponse class containing the list of all entities
	 *         persisted and a cursor to the next page.
	 * @throws UnauthorizedException 
	 */
	@SuppressWarnings({ "unchecked", "unused" })
	@ApiMethod(name = "listIhMCustomer")
	public CollectionResponse<IhMCustomer> listIhMCustomer(
			@Nullable @Named("cursor") String cursorString,
			@Nullable @Named("limit") Integer limit, User user) throws UnauthorizedException {
		if(user == null) throw new UnauthorizedException("User is not valid");
		EntityManager mgr = null;
		Cursor cursor = null;
		List<IhMCustomer> execute = null;

		try {
			mgr = getEntityManager();
			Query query = mgr
					.createQuery("select from IhMCustomer as IhMCustomer");
			if (cursorString != null && cursorString != "") {
				cursor = Cursor.fromWebSafeString(cursorString);
				query.setHint(JPACursorHelper.CURSOR_HINT, cursor);
			}

			if (limit != null) {
				query.setFirstResult(0);
				query.setMaxResults(limit);
			}

			execute = (List<IhMCustomer>) query.getResultList();
			cursor = JPACursorHelper.getCursor(execute);
			if (cursor != null)
				cursorString = cursor.toWebSafeString();

			// Tight loop for fetching all entities from datastore and
			// accomodate
			// for lazy fetch.
			for (IhMCustomer obj : execute)
				;
		} finally {
			mgr.close();
		}

		return CollectionResponse.<IhMCustomer> builder().setItems(execute)
				.setNextPageToken(cursorString).build();
	}

	/**
	 * This method gets the entity having primary key id. It uses HTTP GET
	 * method.
	 *
	 * @param id
	 *            the primary key of the java bean.
	 * @return The entity with primary key id.
	 * @throws UnauthorizedException 
	 */
	@ApiMethod(name = "getIhMCustomer")
	public IhMCustomer getIhMCustomer(@Named("id") Long id, User user) throws UnauthorizedException {
		if(user == null) throw new UnauthorizedException("User is not valid");
		EntityManager mgr = getEntityManager();
		IhMCustomer ihmcustomer = null;
		try {
			ihmcustomer = mgr.find(IhMCustomer.class, id);
		} finally {
			mgr.close();
		}
		return ihmcustomer;
	}

	/**
	 * This inserts a new entity into App Engine datastore. If the entity
	 * already exists in the datastore, an exception is thrown. It uses HTTP
	 * POST method.
	 *
	 * @param ihmcustomer
	 *            the entity to be inserted.
	 * @return The inserted entity.
	 * @throws UnauthorizedException 
	 */
	@ApiMethod(name = "insertIhMCustomer")
	public IhMCustomer insertIhMCustomer(IhMCustomer ihmcustomer, User user) throws UnauthorizedException {
		if(user == null) throw new UnauthorizedException("User is not valid");
		EntityManager mgr = getEntityManager();
		try {
			if (ihmcustomer.get_id() != null) {
				if (containsIhMCustomer(ihmcustomer)) {
					throw new EntityExistsException("Object already exists");
				}
			}
			mgr.persist(ihmcustomer);
		} finally {
			mgr.close();
		}
		return ihmcustomer;
	}

	/**
	 * This method is used for updating an existing entity. If the entity does
	 * not exist in the datastore, an exception is thrown. It uses HTTP PUT
	 * method.
	 *
	 * @param ihmcustomer
	 *            the entity to be updated.
	 * @return The updated entity.
	 * @throws UnauthorizedException 
	 */
	@ApiMethod(name = "updateIhMCustomer")
	public IhMCustomer updateIhMCustomer(IhMCustomer ihmcustomer, User user) throws UnauthorizedException {
		if(user == null) throw new UnauthorizedException("User is not valid");
		EntityManager mgr = getEntityManager();
		try {
			if (!containsIhMCustomer(ihmcustomer)) {
				throw new EntityNotFoundException("Object does not exist");
			}
			mgr.persist(ihmcustomer);
		} finally {
			mgr.close();
		}
		return ihmcustomer;
	}

	/**
	 * This method removes the entity with primary key id. It uses HTTP DELETE
	 * method.
	 *
	 * @param id
	 *            the primary key of the entity to be deleted.
	 * @throws UnauthorizedException 
	 */
	@ApiMethod(name = "removeIhMCustomer")
	public void removeIhMCustomer(@Named("id") Long id, User user) throws UnauthorizedException {
		if(user == null) throw new UnauthorizedException("User is not valid");
		EntityManager mgr = getEntityManager();
		try {
			IhMCustomer ihmcustomer = mgr.find(IhMCustomer.class, id);
			mgr.remove(ihmcustomer);
		} finally {
			mgr.close();
		}
	}

	private boolean containsIhMCustomer(IhMCustomer ihmcustomer) {
		EntityManager mgr = getEntityManager();
		boolean contains = true;
		try {
			IhMCustomer item = mgr
					.find(IhMCustomer.class, ihmcustomer.get_id());
			if (item == null) {
				contains = false;
			}
		} finally {
			mgr.close();
		}
		return contains;
	}

	private static EntityManager getEntityManager() {
		return EMF.get().createEntityManager();
	}

}
