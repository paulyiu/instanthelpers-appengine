package vtc.project.instanthelpers;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class IhMOrder {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long _id;
	private Long mQuotationId;
	private Long mRequestId;
	private Long mHelperId;
	private Long mWorkerId;
	private String mStatus;
	private long timestamp = System.currentTimeMillis();

	public Long getmRequestId() {
		return mRequestId;
	}

	public void setmRequestId(Long mRequestId) {
		this.mRequestId = mRequestId;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public IhMOrder() {
		super();
	}

	public Long get_id() {
		return _id;
	}

	public Long getmQuotationId() {
		return mQuotationId;
	}
	public void setmQuotationId(Long mQuotationId) {
		this.mQuotationId = mQuotationId;
	}
	public Long getmHelperId() {
		return mHelperId;
	}
	public void setmHelperId(Long mHelperId) {
		this.mHelperId = mHelperId;
	}
	public Long getmWorkerId() {
		return mWorkerId;
	}
	public void setmWorkerId(Long mWorkerId) {
		this.mWorkerId = mWorkerId;
	}
	public String getmStatus() {
		return mStatus;
	}
	public void setmStatus(String mStatus) {
		this.mStatus = mStatus;
	}
	
}
